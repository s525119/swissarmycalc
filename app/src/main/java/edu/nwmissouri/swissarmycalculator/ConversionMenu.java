package edu.nwmissouri.swissarmycalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ConversionMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversionmenu);

        // Initializing the buttons
        Button arithmeticBTN = (Button) findViewById(R.id.distanceBTN);
        Button tipBTN = (Button) findViewById(R.id.tempBTN);
        Button bmiBTN = (Button) findViewById(R.id.weightBTN);
        Button timeBTN = (Button) findViewById(R.id.energyBTN);
        Button interestBTN = (Button) findViewById(R.id.volumeBTN);
        Button conversionsBTN = (Button) findViewById(R.id.speedBTN);
        final String[] distance = new String[]{"Kilometers", "Meters", "Centimeters", "Millimeter", "Mile", "Yard", "Feet", "Inch", "Nautical Mile"};
        final double[][] distanceConversion = new double[][]{{1, 1000, 100000, 1000000, 0.621371, 1093.61, 3280.84, 39370.1, 0.539957}, {0.001, 1, 100, 1000, 0.000621371, 1.09361, 3.28084, 39.3701, 0.000539957}, {0.000001, 0.01, 1, 10, .0000062137, 0.0109361, 0.0328084, 0.393701, 0.0000053996}, {.000001, 0.001, 0.1, 1, 0.00000062137, 0.00109361, 0.00328084, 0.0393701, 0.00000053996}, {1.60934, 1609.34, 160934, 1609000, 1, 1760, 5280, 63360, 0.868976}, {0.0009144, 0.9144, 91.44, 914.4, 0.000868182, 1, 3, 36, 0.000493737}, {0.0003048, 0.3048, 30.48, 304.8, 0.000189394, 0.333333, 1, 12, 0.000164576}, {0.0000254, .0254, 2.54, 25.4, 0.000015783, 0.0277778, 0.0833333, 1, 0.000013715}, {1.852, 1852, 185200, 1852000, 1.15078, 2025.37, 6076.12, 72913.4, 1}};
        final String[] temp = new String[]{"Fahrenheit", "Celcius", "Kelvin"};

        final String[] weight = new String[]{"Kilogram", "Gram", "Milligram", "US ton", "Stone", "Pound", "Ounce"};
        final double[][] weightConversion = new double[][]{{1, 1000, 1000000, 0.00110231, 0.157473, 2.20462, 35.274}, {0.001, 1, 1000, 0.0000011023, 0.000157473, 0.00220462, 0.035274}, {1000000, 0.001, 1, 0.0000000011023, 0.00000015747, 0.0000022046, 0.000035274}, {907.185, 907185, 907200000, 1, 142.857, 2000, 32000}, {6.35029, 6350.29, 6350000, .007, 1, 14, 224}, {0.453592, 453.592, 453592, 0.0005, 0.0714286, 1, 16}, {0.0283495, 28.3495, 28349.5, 0.00003125, 0.00446429, 0.0625, 1}};
        final String[] energy = new String[]{"Joule", "Kilojoule", "Gram galorie", "Kilocalorie", "Watt hour", "Kilowatt hour", "British thermal unit", "US therm", "Foot-pound"};
        final double[][] energyConversion = new double[][]{{1, 0.001, 0.239006, 0.000239006, 0.00277778, 0.00000027778, 0.000947817, 0.0000000094804, 0.737562}, {1000, 1, 239.006, 0.239006, 0.277778, 0.000277778, 0.947817, 0.0000094804, 737.562}, {4.184, 0.004184, 1, 0.001, 0.00116222, 0.0000011622, 0.00396567, 0.00000039666, 3.08596}, {4184, 4.184, 1000, 1, 1.16222, 0.00116222, 3.96567, 0.000039666, 3085.96}, {3600, 3.6, 860.421, 0.860421, 1, 0.001, 3.41214, 0.00003413, 2655.22}, {3600000, 3600, 860421, 860.421, 1000, 1, 3412.14, 0.0341296, 2655000}, {1055.06, 1.05506, 252.164, 0.252164, 0.293071, 0.000293071, 1, .000010002, 778.169}, {105500000, 105480, 25210000, 25210.4, 29300.1, 29.3001, 99976.1, 1, 77800000}, {1.35582, 0.00135582, 0.324048, 0.000324048, 0.000376616, 0.00000037662, 0.00128507, 0.000000012854, 1}};
        final String[] volume = new String[]{"US liquid gallon", "US liquid quart", "US liquid pint", "US legal cup", "US fluid ounce", "US tablespoon", "US teaspoon", "Cubic meter", "Liter", "Mililiter", "Cubic foot", "Cubic inch"};
        final double[][] volumeConversion = new double[][]{{1, 4, 8, 15.7726, 127.998, 255.996, 767.988, 0.00378541, 3.78541, 3785.41, 0.133681, 231.001}, {0.25, 1, 2, 4, 32, 64, 192, 0.000946353, 0.946353, 946.353, 0.0334201, 57.7502}, {0.125, 0.5, 1, 2, 16, 32, 96, 0.0000473176, 0.473176, 473.176, 0.0167101, 28.8751}, {0.0634013, 0.253605, 0.50721, 1, 16.2305, 48.6914, 0.00024, 0.24, 240, 0.00847552, 14.6458}, {0.00781262, 0.0312505, 0.062501, 0.123225, 1, 2, 6, 0.00002957, 0.029574, 29.574, 0.0010444, 1.80472}, {0.00390631, 0.0156252, 0.312505, 0.0616125, 0.5, 1, 3, 0.000014787, 0.014787, 14.787, 0.000522198, 0.902362}, {0.0013021, 0.00520842, 0.0104168, 0.0205375, 0.166667, 0.333333, 1, 0.000004929, 0.004929, 4.929, 0.000174066, 0.300787}, {264.172, 1059.69, 2113.38, 4166.67, 33813.5, 67627, 202881, 1, 1000, 1000000, 35.3147, 61024}, {0.294172, 1.05669, 2.11338, 4.1667, 33.8135, 67.627, 202.881, 0.001, 1, 1000, 0.0353147, 61.024}, {0.000264172, 0.00105669, 0.00211338, 0.00416667, 0.0338135, 0.067627, 0.202881, 1000000, .001, 1, .000035315, 0.061024}, {7.48052, 29.9221, 59.8442, 116, 957.491, 1915, 5745, 0.0283168, 28.3168, 28316.8, 1, 1728.01}, {0.00432899, 0.0173159, 0.0346319, 0.0682792, 0.554102, 1.1082, 3.32461, 0.000016387, 0.016387, 16.387, 0.00057870, 1}};
        final String[] speed = new String[]{"Miles per hour", "Foot per second", "Meter per second", "Kilometer per hour", "Knot"};
        final double[][] speedConversion = new double[][]{{1, 1.46667, 0.44704, 1.60934, 0.868976}, {0.681818, 1, 0.3048, 1.09728, 0.592484}, {2.23694, 3.28084, 1, 3.6, 1.94384}, {0.621371, 0.911344, 0.277778, 1, 0.539957}, {1.15075, 1.68781, 0.514444, 1.852, 1}};

        arithmeticBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", distance);
                ini.putExtra("StringConversion", distanceConversion);
                startActivity(ini);
            }
        });

        tipBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", temp);

                startActivity(ini);
            }
        });

        bmiBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", weight);
                ini.putExtra("StringConversion", weightConversion);
                startActivity(ini);
            }
        });

        timeBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", energy);
                ini.putExtra("StringConversion", energyConversion);
                startActivity(ini);
            }
        });

        interestBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", volume);
                ini.putExtra("StringConversion", volumeConversion);
                startActivity(ini);
            }
        });

        conversionsBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ini = new Intent(v.getContext(), Conversions.class);

                ini.putExtra("StringArray", speed);
                ini.putExtra("StringConversion", speedConversion);
                startActivity(ini);
            }
        });
    }
}