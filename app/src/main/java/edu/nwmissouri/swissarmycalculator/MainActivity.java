package edu.nwmissouri.swissarmycalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initializing the buttons
        Button arithmeticBTN = (Button) findViewById(R.id.arithmeticBTN);
        Button tipBTN = (Button) findViewById(R.id.tipBTN);
        Button bmiBTN = (Button) findViewById(R.id.bmiBTN);
        Button timeBTN = (Button) findViewById(R.id.timeBTN);
        Button interestBTN = (Button) findViewById(R.id.interestBTN);
        Button conversionsBTN = (Button) findViewById(R.id.conversionsBTN);

        // Setting onClick listeners
        arithmeticBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Arithmetic.class));
            }
        });

        tipBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Tip.class));
            }
        });

        bmiBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), BMI.class));
            }
        });

        timeBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Time.class));
            }
        });

        interestBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Interest.class));
            }
        });

        conversionsBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), ConversionMenu.class));
            }
        });
    }
}