package edu.nwmissouri.swissarmycalculator;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class BMI extends AppCompatActivity {

    public String spintext;
    protected TextView mBmiResultView;
    protected TextView mBmiMessageView;
    protected EditText mHeightInputView;
    protected EditText mWeightInputView;
    private Spinner heightspin;
    private Spinner weightspin;
    private EditText feet;
    private EditText inch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        mBmiResultView = (TextView) findViewById(R.id.tv_results_bmi);
        mBmiMessageView = (TextView) findViewById(R.id.tv_results_text);
        mHeightInputView = (EditText) findViewById(R.id.cm);
        mWeightInputView = (EditText) findViewById(R.id.et_weight);

        // For horizontal animation to work
        mBmiResultView.setSelected(true);

        // TextWatcher to call the calculation methods when values changes
        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                performCalculations();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        mHeightInputView.addTextChangedListener(tw);
        mWeightInputView.addTextChangedListener(tw);
        heightspin = (Spinner) findViewById(R.id.heightspin);
        weightspin = (Spinner) findViewById(R.id.weightspin);
        feet = (EditText) findViewById(R.id.feet);
        inch = (EditText) findViewById(R.id.inch);
        spintext = heightspin.getSelectedItem().toString();
        ArrayAdapter<CharSequence> weightAdapter = ArrayAdapter.createFromResource(this,
                R.array.weight_arrays, android.R.layout.simple_spinner_item);
        weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weightspin.setAdapter(weightAdapter);


        // setting placeholder texts
        weightspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mWeightInputView.setHint("Weight in lbs.");
                } else {
                    mWeightInputView.setHint("Weight in kg");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.height_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        heightspin.setAdapter(adapter);

        // hiding input fields based on item unit selection
        heightspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    feet.setVisibility(View.VISIBLE);
                    inch.setVisibility(View.VISIBLE);
                    mHeightInputView.setVisibility(View.GONE);
                } else {
                    feet.setVisibility(View.GONE);
                    inch.setVisibility(View.GONE);
                    mHeightInputView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    protected void performCalculations() {
        try {
            double height = getSelectedHeight();
            double weight = getSelectedWeight();
            double bmi = calculateBmi(weight, height);
            mBmiResultView.setText(String.format("%.1f", bmi));
            if (bmi < 18.5) {
                mBmiMessageView.setText(R.string.label_underweight_0);
            } else if (bmi >= 18.5 && bmi < 25) {
                mBmiMessageView.setText(R.string.label_normal);
            } else if (bmi >= 25 && bmi < 30) {
                mBmiMessageView.setText(R.string.label_overweight);
            } else if (bmi >= 30 && bmi < 35) {
                mBmiMessageView.setText(R.string.label_obese_0);
            } else if (bmi >= 35 && bmi < 40) {
                mBmiMessageView.setText(R.string.label_obese_1);
            } else {
                mBmiMessageView.setText(R.string.label_obese_2);
            }
        } catch (NumberFormatException nfe) {
            mBmiResultView.setText("0.0");
            mBmiMessageView.setText(R.string.label_enter_weight_height);
        }
    }

    // retrieve the weight from the spinner control converted to kg
    public double getSelectedWeight() {
        String selectedWeightValue = mWeightInputView.getText().toString();
        if (weightspin.getSelectedItemPosition() == 0) {
            // the position is libs, so convert to kg and return
            return Double.parseDouble(selectedWeightValue) * 0.45359237;
        } else {
            // already kg is selected, so no need to covert (just cast to float)
            return Double.parseDouble(selectedWeightValue);
        }
    }

    // retrieve the height from the spinner control convented to me
    public double getSelectedHeight() {
        String selectedHeightValue = mHeightInputView.getText().toString();
        if (heightspin.getSelectedItemPosition() == 0) {
            // the position is feets and inches, so convert to meters and return
            String feets = String.valueOf(feet.getText().toString());
            String inches = String.valueOf(inch.getText().toString());
            return Double.parseDouble(feets) * 0.3048 +
                    Double.parseDouble(inches) * 0.0254;
        } else {
            // already meters is selected, so no need to covert (just cast to float)
            return Double.parseDouble(selectedHeightValue) / 100;
        }
    }

    // final calculation value of bmi
    protected double calculateBmi(double weightInKg, double heightInMeter) {
        return weightInKg / (heightInMeter * heightInMeter);
    }
}