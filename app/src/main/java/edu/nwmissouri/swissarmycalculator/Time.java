package edu.nwmissouri.swissarmycalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

// Time Calc
public class Time extends AppCompatActivity {


    private TextView sunriseTV;
    private TextView sunsetTV;
    private int sunrise_emoji = 0x1F304;
    private int sunset_emoji = 0x1F307;
    private int sun_emoji = 0x1F31E;
    private TextView sunrise_text;
    private TextView sunset_text;
    private TextView result_text;
    private TextView resultTV;
    private TextView cityName;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        resultTV = (TextView) findViewById(R.id.resultTV);
        sunriseTV = (TextView) findViewById(R.id.sunriseTV);
        sunsetTV = (TextView) findViewById(R.id.sunsetTV);

        sunrise_text = (TextView) findViewById(R.id.sunrise_text);
        sunset_text = (TextView) findViewById(R.id.sunset_text);
        result_text = (TextView) findViewById(R.id.result_text);

        cityName = (TextView) findViewById(R.id.cityName);

        sunrise_text.setText("Sunrise Today " + (new String(Character.toChars(sunrise_emoji))));
        sunset_text.setText("Sunset Today " + (new String(Character.toChars(sunset_emoji))));
        result_text.setText("Amount of Daylight " + (new String(Character.toChars(sun_emoji))));

        // horizontal animation
        resultTV.setSelected(true);


        url = "http://api.openweathermap.org/data/2.5/weather?zip=64468,us&appid=7f0b82454a4c72465ae438c3d4eeb395";


        handleClick(null);
    }


    public void handleClick(View v) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String json = JSONWeatherParser
                        .fetchJSONFromURL(url);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            parseJSON(json);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        }).start();
    }


    public void parseJSON(String json) throws ParseException {
        long sunrise = 0;
        long sunset = 0;

        try {
            JSONObject weatherMap = new JSONObject(json);

            // Loop to parse array nested inside object.
            for (int a = 0; a < weatherMap.length(); a++) {
                JSONObject sys = weatherMap.getJSONObject("sys");
                for (int b = 0; b < sys.length(); b++) {
                    sunrise = sys.getLong("sunrise");
                    sunset = sys.getLong("sunset");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String city = null;

        try {
            JSONObject cityName = new JSONObject(json);
            city = cityName.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //setting the city name

        cityName.setText(city);

        //setting sunriseTV to the value given from the JSON
        Date sunriseDate = new Date(sunrise * 1000);
        DateFormat sunriseFormat = new SimpleDateFormat("HH:mm:ss");
        sunriseFormat.setTimeZone(TimeZone.getTimeZone("Etc/GMT-5"));
        String sunriseformatted = sunriseFormat.format(sunriseDate);
        sunriseTV.setText(sunriseformatted);
        sunriseFormat.setTimeZone(TimeZone.getTimeZone("US/Central"));
        sunriseformatted = sunriseFormat.format(sunriseDate);
        sunriseTV.setText("" + sunriseformatted);

        //setting sunriseTV to the value given from the JSON
        Date sunsetDate = new Date(sunset * 1000);
        DateFormat sunsetFormat = new SimpleDateFormat("HH:mm:ss");
        sunsetFormat.setTimeZone(TimeZone.getTimeZone("Etc/GMT-5"));
        String formatted = sunsetFormat.format(sunsetDate);
        sunsetTV.setText(formatted);
        sunsetFormat.setTimeZone(TimeZone.getTimeZone("US/Central"));
        formatted = sunsetFormat.format(sunsetDate);
        sunsetTV.setText("" + formatted);


        Long difference = (sunriseDate.getTime() - sunsetDate.getTime());
        DateFormat diffFormat = new SimpleDateFormat("HH:mm:ss");
        diffFormat.setTimeZone(TimeZone.getTimeZone("Etc/GMT-5"));
        String diffFormatted = diffFormat.format(difference);

        resultTV.setText((diffFormatted) + "");
    }


}
