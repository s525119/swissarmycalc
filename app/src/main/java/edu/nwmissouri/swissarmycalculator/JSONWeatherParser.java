package edu.nwmissouri.swissarmycalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by mprogers on 10/20/15.
 */
public class JSONWeatherParser {
    static {
    }
    /***
     * @param urlstr the string of the url to return -- JSON source or anything else
     * @return the contents of the url (webpage, JSON, XML, whatever, as a String
     */
    public static String fetchJSONFromURL(String urlstr) {
        StringBuilder jsonStrBuilder = new StringBuilder();
        try {
            URL url = new URL(urlstr);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = br.readLine()) != null)
                jsonStrBuilder.append(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonStrBuilder.toString();
    }
    public static String fetchJSONFromURL(URL url) {

        StringBuilder jsonStrBuilder = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = br.readLine()) != null)
                jsonStrBuilder.append(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonStrBuilder.toString();
    }
}