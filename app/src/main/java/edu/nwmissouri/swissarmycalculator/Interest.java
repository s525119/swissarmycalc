package edu.nwmissouri.swissarmycalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class Interest extends AppCompatActivity {

    //making buttons available for class
    private EditText loanAmount;
    private EditText annualRate;
    private EditText payPeriod;
    private TextView interestET;
    private TextView totalCost;
    private double loan;
    private double rate;
    private double period;
    private double interest;
    private double finalCost;
    private DecimalFormat dFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest);

        //initializing Variables when activity starts
        loanAmount = (EditText) findViewById(R.id.loanAmountET);
        annualRate = (EditText) findViewById(R.id.interestRateET);
        payPeriod = (EditText) findViewById(R.id.payPeriodET);
        interestET = (TextView) findViewById(R.id.interestET);
        totalCost = (TextView) findViewById(R.id.total_to_pay_result);
        dFormat = new DecimalFormat(".##");
    }

    public void calculate(View v) {
        //retrieving data from EditTexts
        try {
            loan = Double.parseDouble(loanAmount.getText().toString());
            rate = Double.parseDouble(annualRate.getText().toString());
            period = Double.parseDouble(payPeriod.getText().toString());
        } catch (Exception e) {
            Log.d("Something", e.toString());
            Toast.makeText(this, "An error occurred.", Toast.LENGTH_SHORT).show();
        }
        interest = 0;
        finalCost = 0;

        //using simple interest formula to calculate TotalCost = P(1+rt)
        //P = loan amount, r = rate, t = time
        finalCost = loan * (1 + (rate * 0.01) * period);
        interest = loan * (rate * 0.01);

        //loanAmount.setText("$ " + loan);
        interestET.setText("$ " + dFormat.format(interest));
        totalCost.setText("$ " + dFormat.format(finalCost));
    }
}

