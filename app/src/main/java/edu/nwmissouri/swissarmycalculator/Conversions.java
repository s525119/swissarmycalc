package edu.nwmissouri.swissarmycalculator;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class Conversions extends AppCompatActivity {

    //numbers used for the calculator
    double number1 = 0.0;
    double number2 = 0.0;
    //true if decimal button has been clicked
    boolean decimal = false;
    boolean decimalUsed = false;
    //true if an operation button has been clicked
    boolean equationPos = false;
    //gets decimal place of current number
    int decimalPlace = 0;
    //gets the equation operation
    // 0 = null, 1 = +, 2 = -, 3 = *, 4 = /
    int equationOperation = 0;
    //Checks to see if an equation has gone through
    boolean equationFinish = false;
    //last save button hit 0 = first one 1 = second one
    int saveBTN = 0;
    //checks if the conversion is temperature
    boolean temperature = false;

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversions);

        //gets conversion data from ConversionMenu
        Bundle ini = getIntent().getExtras();
        final String[] conversionString = ini.getStringArray("StringArray");
        Log.d("TAG", conversionString[0]);
        if (conversionString[0].equals("Fahrenheit")) {
            temperature = true;
        }
        Log.d("TAG", "" + temperature);
        final double[][] conversionDouble = (double[][]) ini.getSerializable("StringConversion");


        final NumberPicker picker1 = (NumberPicker) findViewById(R.id.topNP);
        final NumberPicker picker2 = (NumberPicker) findViewById(R.id.bottomNP);

        picker1.setMinValue(0);
        picker2.setMinValue(0);

        //sets picker max value to be one less than the length of the number of measurments
        picker1.setMaxValue(conversionString.length - 1);
        picker2.setMaxValue(conversionString.length - 1);

        picker1.setDisplayedValues(conversionString);
        picker2.setDisplayedValues(conversionString);

        picker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (oldVal != newVal) {
                    conversionCheck(2);
                }
            }
        });
        picker2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (oldVal != newVal) {
                    conversionCheck(1);
                }
            }
        });

        //converts EditText 2 once EditText 1 has been de-selected
        findViewById(R.id.Input_1ET).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    conversionCheck(1);
                }
            }
        });

        //converts EditText 1 once EditText 2 has been de-selected
        findViewById(R.id.Input_2ET).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    conversionCheck(2);
                }
            }
        });

    }

    public void conversionCheck(int selection) {
        Bundle ini = getIntent().getExtras();
        final String[] conversionString = ini.getStringArray("StringArray");
        final double[][] conversionDouble = (double[][]) ini.getSerializable("StringConversion");
        final NumberPicker pickerEditing;
        final NumberPicker pickerEditor;
        if (selection == 1) {
            pickerEditor = (NumberPicker) findViewById(R.id.topNP);
            pickerEditing = (NumberPicker) findViewById(R.id.bottomNP);
        } else {
            pickerEditing = (NumberPicker) findViewById(R.id.topNP);
            pickerEditor = (NumberPicker) findViewById(R.id.bottomNP);
        }
        int value1 = pickerEditor.getValue();
        int value2 = pickerEditing.getValue();
        Log.d("TAG", "value 1: " + value1 + " value 2: " + value2);
        String conversion1 = conversionString[value1];
        String conversion2 = conversionString[value2];


        EditText inputEditing;
        EditText inputEditor;

        //conversion value
        double conversion;
        //go from input1 to input2
        //if edittext 1's saveButton was last selected
        if (selection == 1) {
            inputEditing = (EditText) findViewById(R.id.Input_2ET);
            inputEditor = (EditText) findViewById(R.id.Input_1ET);
        } else {
            inputEditing = (EditText) findViewById(R.id.Input_1ET);
            inputEditor = (EditText) findViewById(R.id.Input_2ET);
        }
        if (!inputEditor.getText().toString().equals("")) {
            //performs conversion
            if (temperature) {
                if (conversion1.equals("Fahrenheit")) {
                    if (conversion2.equals("Fahrenheit")) {
                        conversion = Double.parseDouble(inputEditor.getText().toString());
                    } else if (conversion2.equals("Celcius")) {
                        conversion = (Double.parseDouble(inputEditor.getText().toString()) - 32) / 1.8;
                    } else {
                        conversion = (Double.parseDouble(inputEditor.getText().toString()) + 459.67) * (.555555556);
                    }
                } else if (conversion1.equals("Celcius")) {
                    if (conversion2.equals("Celcius")) {
                        conversion = Double.parseDouble(inputEditor.getText().toString());
                    } else if (conversion2.equals("Fahrenheit")) {
                        conversion = (Double.parseDouble(inputEditor.getText().toString()) * 1.8) + 32;
                    } else {
                        conversion = Double.parseDouble(inputEditor.getText().toString()) + 273.15;
                    }
                } else {
                    if (conversion2.equals("Kelvin")) {
                        conversion = Double.parseDouble(inputEditor.getText().toString());
                    } else if (conversion2.equals("Celcius")) {
                        conversion = (Double.parseDouble(inputEditor.getText().toString())) - 273.15;
                    } else {
                        conversion = (Double.parseDouble(inputEditor.getText().toString()) / .555555556) - 459.67;
                    }
                }
            } else {
                conversion = conversionDouble[value1][value2] * Double.parseDouble(inputEditor.getText().toString());
            }
            Log.d("TAG", "input1 if statemtent");
            //finds how many decimal places are in the double
            String decimalPlaces = Double.toString(conversion);
            int digitsAfterDecimal = decimalPlaces.length() - decimalPlaces.indexOf(".");
            if (digitsAfterDecimal > 2) {
                digitsAfterDecimal = 5;
            }
            inputEditing.setText(String.format("%." + digitsAfterDecimal + "f", conversion));

        }
    }


//        //converts EditText 2 once EditText 1 has been de-selected
//        ((EditText) findViewById(R.id.Input_1ET)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    int value1 = picker1.getValue();
//                    int value2 = picker2.getValue();
//
//                    String conversion1 = conversionString[value1];
//                    String conversion2 = conversionString[value2];
//
//                    EditText input1 = (EditText) findViewById(R.id.Input_1ET);
//                    EditText input2 = (EditText) findViewById(R.id.Input_2ET);
//
//                    //conversion value
//                    double conversion;
//                    //go from input1 to input2
//                    //if edittext 1's saveButton was last selected
//
//                        if (!input1.getText().toString().equals("")) {
//                            //performs conversion
//                            if (temperature) {
//                                if (conversion1.equals("Fahrenheit")) {
//                                    if (conversion2.equals("Fahrenheit")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Celcius")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString()) - 32)/1.8;
//                                    }else{
//                                        conversion = (Double.parseDouble(input1.getText().toString()) + 459.67)*(.555555556);
//                                    }
//                                }else if (conversion1.equals("Celcius")) {
//                                    if (conversion2.equals("Celcius")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Fahrenheit")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString())*1.8)+32;
//                                    }else{
//                                        conversion = Double.parseDouble(input1.getText().toString()) + 273.15;
//                                    }
//                                }else {
//                                    if (conversion2.equals("Kelvin")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Celcius")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString())) - 273.15;
//                                    }else{
//                                        conversion = (Double.parseDouble(input1.getText().toString())/.555555556) - 459.67;
//                                    }
//                                }
//                            }else {
//                                conversion = conversionDouble[value1][value2] * Double.parseDouble(input1.getText().toString());
//                            }
//                            Log.d("TAG", "input1 if statemtent");
//                            //finds how many decimal places are in the double
//                            String decimalPlaces = Double.toString(conversion);
//                            int digitsAfterDecimal = decimalPlaces.length() - decimalPlaces.indexOf(".");
//                            if (digitsAfterDecimal > 2) {
//                                digitsAfterDecimal = 2;
//                            }
//                            input2.setText(String.format("%." + digitsAfterDecimal + "f", conversion));
//
//                        }
//                    }
//                }
//
//        });
//
//        //does the same as the upper one but for EditText 2
//        ((EditText) findViewById(R.id.Input_2ET)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    int value1 = picker1.getValue();
//                    int value2 = picker2.getValue();
//
//                    String conversion1 = conversionString[value1];
//                    String conversion2 = conversionString[value2];
//
//                    EditText input1 = (EditText) findViewById(R.id.Input_1ET);
//                    EditText input2 = (EditText) findViewById(R.id.Input_2ET);
//                    //go from input2 to input1
//
//                    //conversion value
//                    double conversion;
//
//
//                        if (!input2.getText().toString().equals("")) {
//                            if (temperature) {
//                                if (conversion1.equals("Fahrenheit")) {
//                                    if (conversion2.equals("Fahrenheit")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Celcius")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString()) - 32)/1.8;
//                                    }else{
//                                        conversion = (Double.parseDouble(input1.getText().toString()) + 459.67)*(.555555556);
//                                    }
//                                }else if (conversion1.equals("Celcius")) {
//                                    if (conversion2.equals("Fahrenheit")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Celcius")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString())*1.8)+32;
//                                    }else{
//                                        conversion = Double.parseDouble(input1.getText().toString()) + 273.15;
//                                    }
//                                }else {
//                                    if (conversion2.equals("Fahrenheit")) {
//                                        conversion = Double.parseDouble(input1.getText().toString());
//                                    }else if(conversion2.equals("Celcius")) {
//                                        conversion = (Double.parseDouble(input1.getText().toString())) - 273.15;
//                                    }else{
//                                        conversion = (Double.parseDouble(input1.getText().toString())/.555555556) - 459.67;
//                                    }
//                                }
//                            }else {
//                                conversion = conversionDouble[value2][value1] * Double.parseDouble(input2.getText().toString());
//                            }
//                            String decimalPlaces = Double.toString(conversion);
//                            int digitsAfterDecimal = decimalPlaces.length() - decimalPlaces.indexOf(".");
//                            if (digitsAfterDecimal > 2) {
//                                digitsAfterDecimal = 2;
//                            }
//                            input1.setText(String.format("%." + digitsAfterDecimal + "f", conversion));
//
//
//                        }
//                    }
//                }
//
//        });


    public void onUpClick(View view) {

        TextView output = (TextView) findViewById(R.id.Output_1TV);
        EditText input1 = (EditText) findViewById(R.id.Input_1ET);
        EditText input2 = (EditText) findViewById(R.id.Input_2ET);

        hideSoftKeyboard(this, view);

        if (saveBTN == 0) {
            if (!decimal) {
                input1.setText(output.getText().toString());
            } else
                input1.setText(output.getText().toString());
        } else {
            if (!decimal) {
                input2.setText(output.getText().toString());
            } else
                input2.setText(output.getText().toString());
        }


    }

    public void on0Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        if (input_1.getText().toString() != "") {
            input_1.append("0");
            if (decimal) {
                decimalPlace += 1;
            } else {
                if (equationPos) {
                    number2 *= 10;
                } else {
                    number1 *= 10;
                }
            }
        }
    }

    public void on1Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("1");
        if (decimal) {
            if (equationPos) {
                number2 += (1 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (1 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 1;
            } else {
                number1 *= 10;
                number1 += 1;
            }
        }
    }

    public void on2Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("2");
        if (decimal) {
            if (equationPos) {
                number2 += (2 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (2 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 2;
            } else {
                number1 *= 10;
                number1 += 2;
            }
        }
    }

    public void on3Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("3");
        if (decimal) {
            if (equationPos) {
                number2 += (3 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (3 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 3;
            } else {
                number1 *= 10;
                number1 += 3;
            }
        }
    }

    public void on4Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("4");
        if (decimal) {
            if (equationPos) {
                number2 += (4 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (4 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 4;
            } else {
                number1 *= 10;
                number1 += 4;
            }
        }
    }

    public void on5Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("5");
        if (decimal) {
            if (equationPos) {
                number2 += (5 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (5 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 5;
            } else {
                number1 *= 10;
                number1 += 5;
            }
        }
    }

    public void on6Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("6");
        if (decimal) {
            if (equationPos) {
                number2 += (6 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (6 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 6;
            } else {
                number1 *= 10;
                number1 += 6;
            }
        }
    }

    public void on7Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("7");
        if (decimal) {
            if (equationPos) {
                number2 += (7 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (7 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
                Log.d("TAG", "7 click decimal " + number1);
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 7;
            } else {
                number1 *= 10;
                number1 += 7;
            }
        }
    }

    public void on8Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("8");
        if (decimal) {
            if (equationPos) {
                number2 += (8 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (8 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 8;
            } else {
                number1 *= 10;
                number1 += 8;
            }
        }
    }

    public void on9Click(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (equationFinish) {
            equationFinish = false;
            input_1.setText("");
        }
        input_1.append("9");
        if (decimal) {
            if (equationPos) {
                number2 += (9 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            } else {
                number1 += (9 / (Math.pow(10, decimalPlace)));
                decimalPlace++;
            }
        } else {
            if (equationPos) {
                number2 *= 10;
                number2 += 9;
            } else {
                number1 *= 10;
                number1 += 9;
            }
        }
    }

    public void onDotClick(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (!decimal) {
            input_1.append(".");
            decimal = true;
            decimalPlace = 1;
            decimalUsed = true;
        }
    }

    public void onAddClick(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (!equationPos) {
            input_1.setText("");
            equationOperation = 1;
            equationPos = true;
            decimal = false;
        }
    }

    public void onSubClick(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (!equationPos) {
            input_1.setText("");
            equationOperation = 2;
            equationPos = true;
            decimal = false;
        }
    }

    public void onMultClick(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (!equationPos) {
            input_1.setText("");
            equationOperation = 3;
            equationPos = true;
            decimal = false;
        }
    }

    public void onDivisClick(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (!equationPos) {
            input_1.setText("");
            equationOperation = 4;
            equationPos = true;
            decimal = false;
        }
    }

    public void onSave1Click(View view) {

        EditText input_1 = (EditText) findViewById(R.id.Input_1ET);

        TextView output_1 = (TextView) findViewById(R.id.Output_1TV);
        saveBTN = 0;
        try {
            if (equationPos) {
                number2 = Double.parseDouble(input_1.getText().toString());
                if (((int) number2 * 10) == (number2 * 10)) {
                    output_1.setText(String.format("%d", (int) number2));
                } else {
                    decimal = true;
                    output_1.setText(String.format("%.2f", number2));
                }
            } else {
                number1 = Double.parseDouble(input_1.getText().toString());
                if (((int) number1 * 10) == (number1 * 10)) {
                    output_1.setText(String.format("%d", (int) number1));
                } else {
                    decimal = true;
                    output_1.setText(String.format("%.2f", number1));
                }
            }
        } catch (NumberFormatException e) {
            Toast.makeText(Conversions.this, "Make sure you have something in the Text Box first", Toast.LENGTH_SHORT).show();
        }

    }

    public void onSave2Click(View view) {

        EditText input_2 = (EditText) findViewById(R.id.Input_2ET);

        TextView output_1 = (TextView) findViewById(R.id.Output_1TV);
        saveBTN = 1;


        try {
            if (equationPos) {
                number2 = Double.parseDouble(input_2.getText().toString());
                if (((int) number2 * 10) == (number2 * 10)) {
                    output_1.setText(String.format("%d", (int) number2));
                } else {
                    output_1.setText(String.format("%.2f", number2));
                }
            } else {

                number1 = Double.parseDouble(input_2.getText().toString());
                if (((int) number1 * 10) == (number1 * 10)) {
                    output_1.setText(String.format("%d", (int) number1));
                } else {
                    output_1.setText(String.format("%.2f", number1));
                }
            }
        } catch (NumberFormatException e) {
            Toast.makeText(Conversions.this, "Make sure you have something in the Text Box first", Toast.LENGTH_SHORT).show();
        }

    }

    public void onClearAll(View view) {
        TextView output_1 = (TextView) findViewById(R.id.Output_1TV);
        EditText input_1 = (EditText) findViewById(R.id.Input_1ET);
        EditText input_2 = (EditText) findViewById(R.id.Input_2ET);
        output_1.setText("");
        input_1.setText("");
        input_2.setText("");
        number1 = 0.0;
        number2 = 0.0;
        decimalPlace = 0;
        decimal = false;
        decimalUsed = false;
        equationPos = false;
        equationOperation = 0;
    }

    public void onEqualClick(View view) {

        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        equationFinish = true;
        if (equationPos) {
            if (input_1.getText().toString() != "") {
                switch (equationOperation) {
                    case 1:

                        number1 += number2;
                        if (decimalUsed) {
                            input_1.setText(String.format("%." + decimalPlace + "f", number1));
                        } else {
                            input_1.setText(String.format("%d", (int) number1));
                        }
                        break;
                    case 2:
                        number1 -= number2;
                        if (decimalUsed) {
                            input_1.setText(String.format("%." + decimalPlace + "f", number1));
                        } else {
                            input_1.setText(String.format("%d", (int) number1));
                        }
                        break;
                    case 3:
                        number1 *= number2;
                        Log.d("TAG", "Mult num1: " + number1 + " num2: " + number2);
                        if (decimalUsed) {
                            input_1.setText(String.format("%." + decimalPlace + "f", number1));
                        } else {
                            input_1.setText(String.format("%d", (int) number1));
                        }
                        break;
                    case 4:
                        Log.d("TAG", "Divi num1: " + number1 + " num2: " + number2);
                        number1 /= number2;
                        double number1DecimalTest = number1;
                        if (!(number1 * 10 == (int) number1 * 10)) {
                            decimalUsed = true;
                            while (!(number1DecimalTest * 10 == (int) number1DecimalTest * 10)) {
                                decimalPlace++;
                                number1DecimalTest *= 10;
                                if (decimalPlace > 5) {
                                    break;
                                }
                            }
                        }
                        if (decimalUsed) {
                            input_1.setText(String.format("%." + decimalPlace + "f", number1));
                        } else {
                            input_1.setText(String.format("%d", (int) number1));
                        }
                        break;
                    default:
                        Toast.makeText(Conversions.this, "Don't forget to choose an operation", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
            equationPos = false;

            number2 = 0.0;
            equationOperation = 0;
        }
    }

    public void on1OverX(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (decimalPlace == 0) {
            decimalPlace += 1;
        }
        if (equationPos) {
            if (number2 == 0) {
                input_1.setText("ERROR");
                Toast.makeText(Conversions.this, "You can't divide by zero", Toast.LENGTH_SHORT).show();
            } else {
                number2 = 1 / number2;
                decimalUsed = true;
                double number2DecimalTest = number2;
                while (!(number2DecimalTest * 10 == (int) number2DecimalTest * 10)) {
                    decimalPlace++;
                    number2DecimalTest *= 10;
                    if (decimalPlace > 5) {
                        break;
                    }
                }
                input_1.setText(String.format("%." + decimalPlace + "f", number2));
            }
        } else {
            if (number1 == 0) {
                input_1.setText("ERROR");
                Toast.makeText(Conversions.this, "You can't divide by zero", Toast.LENGTH_SHORT).show();
            } else {
                number1 = 1 / number1;
                decimalUsed = true;
                double number1DecimalTest = number1;
                while (!(number1DecimalTest * 10 == (int) number1DecimalTest * 10)) {
                    decimalPlace++;
                    number1DecimalTest *= 10;
                    if (decimalPlace > 5) {
                        break;
                    }
                }
                input_1.setText(String.format("%." + decimalPlace + "f", number1));
            }
        }
    }

    public void onSqrt(View view) {
        TextView input_1 = (TextView) findViewById(R.id.Output_1TV);
        if (decimalPlace == 0) {
            decimalPlace += 1;
            decimal = true;
        }
        if (equationPos) {
            if (number2 >= 0) {
                number2 = Math.sqrt(number2);

                decimalUsed = true;
                double number2DecimalTest = number2;
                while (!(number2DecimalTest * 10 == (int) number2DecimalTest * 10)) {
                    decimalPlace++;
                    number2DecimalTest *= 10;
                    if (decimalPlace > 5) {
                        break;
                    }
                }
                input_1.setText(String.format("%." + decimalPlace + "f", number2));
            } else {
                Toast.makeText(Conversions.this, "The number cannot be negative", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (number1 >= 0) {
                number1 = Math.sqrt(number1);
                decimalUsed = true;
                double number1DecimalTest = number1;
                while (!(number1DecimalTest * 10 == (int) number1DecimalTest * 10)) {
                    decimalPlace++;
                    number1DecimalTest *= 10;
                    if (decimalPlace > 5) {
                        break;
                    }
                }
                input_1.setText(String.format("%." + decimalPlace + "f", number1));
            } else {
                Toast.makeText(Conversions.this, "The number cannot be negative", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
