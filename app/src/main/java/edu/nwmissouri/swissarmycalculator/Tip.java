package edu.nwmissouri.swissarmycalculator;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Tip extends AppCompatActivity {

    private EditText totalBillAmount;
    private SeekBar tipPercent;
    private SeekBar tipsNumberOfPeople;
    private TextView totalAmountToBePaid;
    private TextView totalAmountOfTipsToBePaid;
    private TextView tipsPerPerson;
    private Button calculateTips;
    private int tipPercentValue = 0;
    private int tipsForNumberOfPeople = 0;
    private TextView tipPercentLabel;
    private TextView splitNumberLabel;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip);
        setupUI(findViewById(R.id.dataRL));
        totalBillAmount = (EditText) findViewById(R.id.bill_value);
        tipPercent = (SeekBar) findViewById(R.id.seekBar);
        tipsNumberOfPeople = (SeekBar) findViewById(R.id.seekBar_one);
        totalAmountToBePaid = (TextView) findViewById(R.id.total_to_pay_result);
        totalAmountOfTipsToBePaid = (TextView) findViewById(R.id.total_tip_result);
        tipsPerPerson = (TextView) findViewById(R.id.tip_per_person_result);
        tipPercentLabel = (TextView) findViewById(R.id.tip_percent);
        splitNumberLabel = (TextView) findViewById(R.id.split_number);
        tipPercent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tipPercentValue = progress;
                tipPercentLabel.setText("Tip Percentage - " + String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //tipPercentLabel.setText("Tip Percent - " + seekBar.getProgress());
            }
        });
        tipsNumberOfPeople.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tipsForNumberOfPeople = progress;
                splitNumberLabel.setText("Number of People - " + tipsForNumberOfPeople);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //splitNumberLabel.setText("Split Number - " + seekBar.getProgress());
            }
        });
        calculateTips = (Button) findViewById(R.id.calculate_tips);
        calculateTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (totalBillAmount.getText().toString().equals("") || totalBillAmount.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter the bill amount", Toast.LENGTH_LONG).show();
                    return;
                }
                double totalBillInput = Double.parseDouble(totalBillAmount.getText().toString());
                if (tipPercentValue == 0) {
                    Toast.makeText(getApplicationContext(), "Please provide the Tip Percentage", Toast.LENGTH_LONG).show();
                    return;
                } else if (tipsForNumberOfPeople == 0) {
                    Toast.makeText(getApplicationContext(), "Please provide the Number of People", Toast.LENGTH_LONG).show();
                    return;
                }

                double percentageOfTip = (totalBillInput * tipPercentValue) / 100;
                double totalAmountForTheBill = totalBillInput + percentageOfTip;
                double tipPerEachPerson = totalAmountForTheBill / tipsForNumberOfPeople;
                totalBillAmount.setText(removeTrailingZero(String.format("%.2f", totalBillInput)));
                totalAmountToBePaid.setText(removeTrailingZero(String.valueOf(String.format("$ %.2f", totalAmountForTheBill))));
                totalAmountOfTipsToBePaid.setText(removeTrailingZero(String.valueOf(String.format("$ %.2f", percentageOfTip))));
                tipsPerPerson.setText(removeTrailingZero(String.valueOf(String.format("$ %.2f", tipPerEachPerson))));
            }
        });
        return;
    }

    public String removeTrailingZero(String formattingInput) {
        if (!formattingInput.contains(".")) {
            return formattingInput;
        }
        int dotPosition = formattingInput.indexOf(".");
        String newValue = formattingInput.substring(dotPosition, formattingInput.length());
        if (newValue.startsWith(".0")) {
            return formattingInput.substring(0, dotPosition);
        }
        return formattingInput;
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Tip.this);
                    return false;
                }

            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
}
